
age_train <- train[which(!is.na(train$Age)),]
age_test <- train[which(is.na(train$Age)),]

age_train <- age_train %.%
     dplyr::select(-PassengerId,-Name,-Ticket,-Cabin,-Survived) %.%
     dplyr::mutate(
          #Survived = factor(Survived),
          Pclass = ordered(Pclass),
          SibSp = ordered(SibSp),
          Parch = ordered(Parch,levels = c(0:6,9))
     )

age_train_tuned <- tuneRF(
     dplyr::select(age_train,-Age),
     age_train$Age,
     trace = TRUE,
     doBest=TRUE
)

age_train_rf <- randomForest(
     Age ~ Pclass + Sex + SibSp + Parch + Fare + Embarked,
     mtry = 2,
     data = age_train, 
     ntree = 1000, 
     proximity = TRUE, 
     importance = TRUE
)

# table(
#      predict(train1_rf),
#      train1$Survived
#      )

#plot(train1_rf)
print(age_train_rf)
varImpPlot(age_train_rf)
importance(age_train_rf)

age_test <- age_test %.%
     dplyr::select(-Survived,-PassengerId,-Name,-Ticket,-Cabin) %.%
     dplyr::mutate(
          #Survived = factor(Survived),
          Pclass = ordered(Pclass),
          SibSp = ordered(SibSp),
          Parch = ordered(Parch,levels = c(0:6,9))
     )

age_pred <- predict(
     age_train_rf, 
     newdata = age_train 
)

ggplot(
     data = age_train,
     aes(
          x = age_train$Age,
          y = age_pred,
          fill = Sex:Pclass,
          alpha = 0.6
     )
) + 
     geom_point(
          shape = 21, 
          col = "darkblue"
     ) + 
     facet_grid(Sex~Pclass)+
     xlab("Observed Age")+
     ylab("Age predicted by randomForest")+
     theme(
          legend.position="none"
     )+
     geom_abline(
          intercept = 0, 
          slope = 1,
          lty = 2,
          colour = "darkblue"
     )

age_pred <- predict(
     age_train_rf, 
     newdata = age_test 
)

### incorporate prediction into full training dataset

train[which(is.na(train$Age)),"Age"] <- round(age_pred,2)
