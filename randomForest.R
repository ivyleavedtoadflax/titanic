
library(randomForest)

data(mtcars)
mtcars

fit <- randomForest(
     mpg ~ cyl + wt + hp, 
     data = mtcars
     )

print(fit) # view results 
importance(fit) # importance of each predictor

