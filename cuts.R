cuts <- function(data, n = 5) {
     
     # check input vectors
     
     test_that(n,is_a("integer"))
     test_that(data,is_a("numeric"))
     
     x <- kmeans(
          na.omit(data), 
          centers = n
          ) 
     
     x <- sort(round(x$centers))
          
     low <- vector(length=length(x))
     high <- vector(length=length(x))
     out <- vector(length=length(x))
     
     for (i in 1:length(x)) {
          
          if (i > 1) {
                    diff <- x[i] - x[i-1]         
                    cut_low <- ceiling(x[i] - diff/2)
               } else
                    if (i == 1) {
                         cut_low <- floor(min(data,na.rm=TRUE))  
                    } 
          
          low[i] <- cut_low
               
     }
     
     for (i in 1:length(x)) {
          
          if (i < length(x)) {
               
               high[i] <- low[i+1]
               
          } 
          
          high[length(x)] <- round(max(data,na.rm=TRUE))
     }
     
     expect_that(low,is_a("numeric"))
     expect_that(high,is_a("numeric"))
     
     out <- paste(low,high,sep="-")
     expect_that(out,is_a("character"))
      
     return(out)
     
}